# BoB

This is the mobile ticketing project space at bitbucket, hosted by Samtrafiken AB.

# Interactive MTB payloadData editor

http://schemas.mobileticket.se/payloadData/

# Source tree
    ├───api: PDF-documents with overview of achitecture and UML-charts (README exists)
    │   └───endpoints: API-endpoints in Swagger 2.0-format.
    ├───spec
    │   ├───current: Released specifications (PDF)
    │   ├───changes: Latest update of specifications (PDF)
    │   └───schema: Schemas for MTB payload
    └───src: Example code snippets
        ├───mtb
        │   ├───examples (Perl-scripts) (README exists)
        │   │   ├───build: Snippet to build and sign complete MTB
        │   │   └───data: Snippet for validating a MTB
        │   └───regress
        │       └───jws: JWS signing and validation with Perl and OpenSSL
        └───tickle: Python prototype TICKLE parser (README exists)
            └───test-cases: Test cases for TICKLE parser