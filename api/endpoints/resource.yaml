swagger: '2.0'
info:
  version: v1
  title: BoB.Resource.API
host: schemas.mobileticket.se
basePath: /api/v1
schemes:
  - https
paths:
  '/resource':
    get:
      tags:
        - resource
      summary: Get available resources
      operationId: getResources
      produces:
        - application/json
      parameters:
        - name: X-BoB-AuthToken
          in: header
          description: JWT authentication token
          required: true
          type: string
        - name: Accept-Language
          in: header
          description: List of preferred languages
          required: false
          type: string
        - in: query
          name: resourceCategoryId
          required: false
          type: string
          description: Filter available resources by resource category
        - in: query
          name: locationId
          type: string
          description: Filter available resources by location
          required: false
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/resources'
          headers:
            Content-Language:
              type: string
  '/resource/resourceCategories':
    get:
      tags:
        - resource
      summary: Get resource categories
      operationId: getResourceCategories
      produces:
        - application/json
      parameters:
        - name: X-BoB-AuthToken
          in: header
          description: JWT authentication token
          required: true
          type: string
        - name: Accept-Language
          in: header
          description: List of preferred languages
          required: false
          type: string
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/resourceCategories'
          headers:
            Content-Language:
              type: string
  '/resource/manifest':
    get:
      tags:
        - resource
      summary: Get resource manifest by resource and option identifiers
      operationId: getResourceManifest
      produces:
        - application/json
      parameters:
        - name: X-BoB-AuthToken
          in: header
          description: JWT authentication token
          required: true
          type: string
        - name: Accept-Language
          in: header
          description: List of preferred languages
          required: false
          type: string
        - name: resourceManifestQuery
          in: body
          description: Requested resources and options
          required: true
          schema:
            $ref: '#/definitions/resourceManifestQuery'
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/resourceManifestSet'
          headers:
            Content-Language:
              type: string
  '/resource/availability':
    get:
      tags:
        - resource
      summary: Find available resources by category, location and time constraints
      operationId: getResourcesByConstraints
      produces:
        - application/json
      parameters:
        - name: X-BoB-AuthToken
          in: header
          description: JWT authentication token
          required: true
          type: string
        - name: Accept-Language
          in: header
          description: List of preferred languages
          required: false
          type: string
        - in: query
          name: resourceCategoryId
          required: false
          type: string
          description: Filter query by category
        - in: query
          name: availabilityFrom
          required: false
          type: string
          description: Filter query by time availability (from)  
        - in: query
          name: availabilityTo
          required: false
          type: string
          description: Filter query by time availability (to)  
        - in: query
          name: locationId
          required: false
          type: string
          description: Filter query by location id  
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/resourceAlternatives'
          headers:
            Content-Language:
              type: string
definitions:
  resourceCategories:
    type: array
    items:
      $ref: '#/definitions/resourceCategory'
  resourceCategory:
    type: object
    properties:
      resourceCategoryId:
        type: string
      resourceCategorydescription:
        type: string
  resourceAlternatives:
    description: alternative resources matching query
    type: array
    items:
      $ref: '#/definitions/resource'
  resourceManifestQuery:
    description: Request for manifest by resourceIds and optionIds
    type: object
    properties:
      resourceIds:
        type: array
        items:
          description: resourceId
          type: string
      optionIds:
        type: array
        items:
          description: optionId
          type: string
  resourceManifestSet:
    description: set of immutable resources
    type: object
    properties:
      resourceSetTitle:
        type: string
      resourceSetDescription:
        type: string
      manifest:
        type: string
        format: byte
      manifestExpiry:
        type: string
        format: date-time
      distinct:
        type: boolean
        description: if manifest is distinct or can be called multiple times
      fare:
        $ref: '#/definitions/fare'
      resources:
        $ref: '#/definitions/resources'
  resources:
    type: array
    items:
      $ref: '#/definitions/resource'
  resource:
    type: object
    properties:
      resourceId:
        type: string
      resourceTitle:
        type: string
      resourceDescription:
        type: string
      resourceLocation:
        type: string
      resourceOptions:
        type: array
        items:
          $ref: '#/definitions/resourceOption'
      fare:
        $ref: '#/definitions/fare'
  resourceOption:
    type: object
    properties:
      optionId:
        type: string
      optionType:
        type: string
        description: optional items tied to the resource, e.g. a child seat
      optionDescription:
        type: string
      optionValue:
        type: string
      surcharge:
        $ref: '#/definitions/fare'
  fare:
    type: object
    required:
      - amount
      - currency
      - vatAmount
      - vatPercent
    properties:
      amount:
        description: Fare cost excluding VAT
        type: number
        format: float
      currency:
        description: Currency code (ISO 4217)
        type: string
      vatAmount:
        description: VAT amount
        type: number
        format: float
      vatPercent:
        description: VAT percent
        type: number
        format: float
