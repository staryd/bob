#!/usr/bin/perl

use strict;
use warnings;
use CBOR::XS;

my $version_file		= shift @ARGV;
my $signedDeviceBundle_file	= shift @ARGV;

my %data;
my $fh;

my $coder = CBOR::XS->new->text_keys;


# read version file
my $version = "";
open($fh, "<", $version_file);
while (<$fh>) {
    $version .= $_;
}
close($fh);

# read signedDeviceBundle
my $signedDeviceBundle = "";
open($fh, "<", $signedDeviceBundle_file);
while (<$fh>) {
    $signedDeviceBundle .= $_;
}
close($fh);

my $version_tag = "v";
my $payload_tag = "p";

%data = ( $version_tag => $version, 
          $payload_tag => $signedDeviceBundle );

# output as CBOR
print $coder->encode(\%data);
