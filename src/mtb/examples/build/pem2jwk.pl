#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;
use CBOR::XS;
use FileHandle;
use IPC::Open2;
use Convert::ASN1;
use MIME::Base64 qw(decode_base64);
use MIME::Base64::URLSafe qw(urlsafe_b64encode);

my $asn1keypriv = Convert::ASN1->new;
$asn1keypriv->prepare(q<

  SEQUENCE {
		v INTEGER,
		d OCTET STRING,
    [0] SEQUENCE {
	    curve OBJECT IDENTIFIER
		},
    [1] SEQUENCE {
      xy BIT STRING
		}
  }

>);

my $asn1keypub = Convert::ASN1->new;
$asn1keypub->prepare(q<

  SEQUENCE {
    SEQUENCE {
	    alg OBJECT IDENTIFIER,
	    curve OBJECT IDENTIFIER
		}
    xy BIT STRING
  }

>);

my $key_file     = shift @ARGV;
my $header_file  = shift @ARGV;
my $privkey_jwk  = shift @ARGV;
my $pubkey_jwk   = shift @ARGV;
my $fh;

my $p256  = "1.2.840.10045.3.1.7";
my $uncompressed = pack("H*", "04");

# read header
my $header = "";
open($fh, "<", $header_file);
while (<$fh>) {
    $header .= $_;
}
close($fh);

# read key (only for jwk output)
my $keyb64 = "";
open($fh, "<", $key_file);
while (<$fh>) {
	  $_ =~ s/^-----.*-----$//g;
    $keyb64 .= $_;
}
close($fh);

my $asn1keystruct = $asn1keypriv->decode(decode_base64($keyb64));

if ( $asn1keystruct->{curve} eq $p256 ) {

  my $form = substr $asn1keystruct->{xy}[0], 0, 1;
	my $x;
	my $y;
	
	if ($form eq $uncompressed) {
		
	  $x = substr $asn1keystruct->{xy}[0], 1, 32;
	  $y = substr $asn1keystruct->{xy}[0], 33, 32;
		
    #printf( "x (hex): %s\n", unpack( "H*", $x ) );
    #printf( "y (hex): %s\n", unpack( "H*", $y ) );
	
    my $h   = decode_cbor($header);
    my $kid = $h->{kid};
	
	
    open(JWK, "> $pubkey_jwk"); 

    printf JWK ( "{\n" );
    printf JWK ( "  \"x\": \"%s\",\n", urlsafe_b64encode($x) );
    printf JWK ( "  \"y\": \"%s\",\n", urlsafe_b64encode($y) );
    printf JWK ( "  \"kid\": \"%s\",\n", $kid );
    printf JWK ( "  \"crv\": \"P-256\",\n" );
    printf JWK ( "  \"kty\": \"EC\"\n" );
    printf JWK ( "}\n" );

    close(JWK);

    open(JWK, "> $privkey_jwk"); 

    printf JWK ( "{\n" );
    printf JWK ( "  \"x\": \"%s\",\n", urlsafe_b64encode($x) );
    printf JWK ( "  \"y\": \"%s\",\n", urlsafe_b64encode($y) );
    printf JWK ( "  \"d\": \"%s\",\n", urlsafe_b64encode($asn1keystruct->{d}) );
    printf JWK ( "  \"kid\": \"%s\",\n", $kid );
    printf JWK ( "  \"crv\": \"P-256\",\n" );
    printf JWK ( "  \"kty\": \"EC\"\n" );
    printf JWK ( "}\n" );

    close(JWK);

	} else {
		
		die "Other than uncompressed form is not supported\n";
		
	}
} else {
	
	die "Other key types than ECDSA P256 are not supported\n";

}
