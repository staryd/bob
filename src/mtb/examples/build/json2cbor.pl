#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;
use JSON;
use CBOR::XS;

my $json = "";

my $coder = CBOR::XS->new->text_strings;

while (<>) {
    $json .= $_;
}

print $coder->encode (JSON->new->allow_nonref->decode($json));
