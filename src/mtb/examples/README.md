# Perl-script to handle MTB

## Getting Started

### To setup perl-enviroment (Tested on CentOS 7)
```bash
sudo cpan
    > Would you like to configure as much as possible automatically? yes
    > What approch do you want? sudo
    > Would you like me to automatically choose some CPAN mirror sites for you? yes
    install JSON
    install Canary::Stability
    install Time::Piece
    install CBOR::XS
    exit
```
> Clean the build-dir under /root/.span if running into problem that makes cpan won't run again:
> sudo rm -r /root/.span/build
 
### To Build example code
```
cd bob/src/mtb/examples/build
make
```

### Example commands: (From build-folder)
```bash
cd bob/src/mtb/examples/build
hexdump -C mtb.bin
perl ../dump-mtb.pl mtb.bin
perl ../dump-mtb.pl mtb.bin --help
perl ../dump-mtb.pl mtb.bin --pubkey key-public.pem
perl ../dump-mtb.pl mtb.bin --pubkey key-public.pem --devkey key-device.bin
```