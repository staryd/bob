#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;
use JSON;
use CBOR::XS;

my $json = "";

while (<>) {
    $json .= $_;
}

print encode_cbor(decode_json($json));

