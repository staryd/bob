#!/usr/bin/perl

use strict ;
use warnings ;
use IO::Uncompress::Inflate qw(inflate $InflateError) ;

my $input = shift @ARGV;

my $output = $input . ".inflated";
inflate $input => $output
  or die "inflate failed: $InflateError\n";
