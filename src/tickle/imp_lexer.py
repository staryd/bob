
import lexer

RESERVED = 'RESERVED'
INT      = 'INT'
FLOAT    = 'FLOAT'
ID       = 'ID'
PROP     = 'PROP'
MACRO    = 'MACRO'
TEMPUS   = 'TEMPUS'

token_exprs = [
    (r'[ \n\t]+',              None),
    (r'//[^\n]*',              None),
    (r'\:=',                   RESERVED),
    (r'\:',                    RESERVED),
    (r'\(',                    RESERVED),
    (r'\)',                    RESERVED),
    (r';',                     RESERVED),
    (r'\+',                    RESERVED),
    (r'-',                     RESERVED),
    (r'\*',                    RESERVED),
    (r'/',                     RESERVED),
    (r'<=',                    RESERVED),
    (r'<<',                    RESERVED),
    (r'<',                     RESERVED),
    (r'>=',                    RESERVED),
    (r'>>',                    RESERVED),
    (r'>',                     RESERVED),
    (r'!=',                    RESERVED),
    (r'=',                     RESERVED),
    (r'\&',                    RESERVED),
    (r'\|',                    RESERVED),
    (r'\!',                    RESERVED),
    (r'\{',                    RESERVED),
    (r'\}',                    RESERVED),
    (r'\[',                    RESERVED),
    (r'\]',                    RESERVED),
    (r',',                     RESERVED),
    (r'@[TRP0-9][A-Za-z0-9\-\+\:\/]*', TEMPUS),
    (r'[0-9]+\.[0-9]+',                FLOAT),
#    (r'[0-9]+',                        INT),
    (r'[A-Za-z0-9_]+:[A-Za-z0-9_]+:[A-Za-z0-9_]+',   PROP),
#    (r'[A-Za-z0-9_]*:[A-Za-z0-9_]+',   PROP),
    (r'#[A-Za-z][A-Za-z0-9_]*',        MACRO),
    (r'[A-Za-z0-9_]*',         ID),
]

def imp_lex(characters):
    return lexer.lex(characters, token_exprs)
