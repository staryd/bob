
from equality import *
import sys
import aniso8601
import json
import math

class Root(Equality):
    def toJson(self, indent=2):
        return json.dumps(self.toObject(), indent)

    def fromBinary(self, key):
        if type(self.left) == self.__class__:
            return {key: self.left.toObject()[key] + [self.right.toObject()]}
        else:
            return {key: [self.left.toObject(), self.right.toObject()]}

# Base class for statements
class Statement(Root):
    pass

# Base class for conditions
class Condition(Root):
    pass

# Macro assign: just add definition to environment
class Definition(Statement):
    def __init__(self, name, condition):
        self.name = name
        self.condition = condition

    def __repr__(self):
        return 'Definition(%s, %s)' % (self.name, self.condition)

    def eval(self, env):
        env[self.name] = self.condition

    def toObject(self):
        return {"name": self.name, "condition": self.condition.toObject()}

# Declaration: if current PID then evaluate expression
class Entitlement(Statement):
    def __init__(self, pid, condition):
        self.pid = pid
        self.condition = condition

    def __repr__(self):
        return 'Entitlement(%s, %s)' % (self.pid, self.condition)

    def eval(self, env):
        value = env['_pid'] == self.pid and self.condition.eval(env)
        env['_result'] = env['_result'] or value 
        if "test" in sys.argv:
            print self.pid, ":", value

    def toObject(self):
        return {"pid": self.pid, "condition": self.condition.toObject()}

# compound statement 
class Statements(Statement):
    def __init__(self, macros, entitlements):
        self.macros = macros
        self.entitlements = entitlements

    def __repr__(self):
        return 'Statements(macros=%s, entitlements=%s)' % (self.macros, self.entitlements)

    def eval(self, env):
        [macro.eval(env) for macro in self.macros]
        [entitlement.eval(env) for entitlement in self.entitlements]

    def toObject(self):
        return {
            "c_ma": [macro.toObject() for macro in self.macros],
            "c_ta": [entitlement.toObject() for entitlement in self.entitlements]
        }

# GeoRadius is defined by center and radius
class GeoRadius(Condition):
    def __init__(self, coordinate, radius):
        self.coordinate = coordinate
        self.radius = radius

    def __repr__(self):
        return 'GeoRadius(%s, %s)' % (self.coordinate, self.radius)

    def eval(self, env):
        if "_location" in env:
            a1 = env["_location"][0]
            a2 = env["_location"][1]
            b1 = self.coordinate[0]
            b2 = self.coordinate[1]
            d1 = (a1-b1)*(a1-b1)
            d2 = (a2-b2)*(a2-b2)
            return math.sqrt(d1+d2) < self.radius
        else:
            return False

    def toObject(self):
        return {"geoRadius": {"coordinate": {"lat": self.coordinate[0], "lon": self.coordinate[1]},
                              "radius": self.radius}}
# Geo-area polygon - check if current location is within defined polygon
class GeoArea(Condition):
    def __init__(self, coordinates):
        self.coordinates = coordinates

    def __repr__(self):
        return 'GeoArea(%s)' % str(self.coordinates)

    def eval(self, env):
        if "_location" in env:
            a1 = env["_location"][0]
            a2 = env["_location"][1]
            return self.coordinates.test(env["_location"])
        else:
            return False

    def toObject(self):
        return {"geoArea": {"coordinates": self.coordinates.toObject()}}

# CoordinateList defines a polygon
# Right now simplified test -- assumes two first coordinates are lower left and upper right corner of rectangle, not general polygon
class CoordinateList(Statement):
    def __init__(self, c1, c2):
        if type(c1) is CoordinateList:
            self.expr = c1.value()+[c2]
        else:
            self.expr = [c1,c2]

    def __repr__(self):
        return 'CoordinateList(%s)' % str(self.expr)

    def value(self):
        return self.expr
    
    def test(self,value):
        a = self.expr[0][0]
        b = self.expr[0][1]
        c = self.expr[1][0]
        d = self.expr[1][1]
        x = value[0]
        y = value[1]
        return a <= x and x <= c and b <= y and y <= d
    
    def eval(self, env):
        return True

    def toObject(self):
        return map(lambda e: {"lat": e[0], "lon": e[1]}, self.expr)

# Property, i.e. a boolean value

class Property(Condition):

    # CURRENTLY NOT USED!
    types = ['zone','line','stop','direction','special']
    
    def __init__(self, pid, typ, values):
        #if not typ.lower() in self.types:
        #    print "ERROR: type",typ,"not in",self.types
        #    sys.exit(-1)
        self.pid = pid
        self.type = typ
        self.values = values

    def __repr__(self):
        return 'Property(%s:%s:[%s])' % (self.pid, self.type, ", ".join(self.values))

    def eval(self, env):
        if "_prop" in env:
            return any(["%s:%s:%s" % (self.pid, self.type.lower(),value) in env["_prop"] for value in self.values])
        else:
            return False

    def toObject(self):
        return {"property": {"pid": self.pid, "type": self.type, "values": self.values}}

# Temporal expressions
class Time(Condition):
    def __init__(self, timeSpec):
        self.time = timeSpec

    def __repr__(self):
        return 'Time(%s)' % self.time

    def eval(self, env):
        if "_now" in env:
            # time intervals (currently only within a day)
            if not self.time[0] in ['R','P'] and len(self.time.split("/")) == 2:
                try:
                    (a,b) = aniso8601.parse_interval(self.time)
                    if "test" in sys.argv:
                        print "TIME INTERVAL 1",a," - ",b,"compared to", env["_now"]
                    res = str(a) <= str(env["_now"]) <= str(b)
                    if "test" in sys.argv:
                        print "T 1 RES",res
                    return res
                except:
                    (a,b) = self.time.split("/")
                    if "test" in sys.argv:
                        print "TIME INTERVAL 2",a," - ",b
                    (ah,am) = a.split(":")
                    (bh,bm) = b.split(":")
                    res = env["_now"].replace(hour=int(ah)).replace(minute=int(am)) <= env["_now"] <= env["_now"].replace(hour=int(bh)).replace(minute=int(bm))
                    if "test" in sys.argv:
                        print "T 2 RES",res
                    return res
            # Repeat
            if self.time[0] == 'R':
                xs = self.time.split("/")
                if len(xs) > 3:
                    # print "REPEAT WITH TIME INTERVALS NOT YET IMPLEMENTED",xs
                    dates = aniso8601.parse_repeating_interval(xs[0]+"/"+xs[1]+"/"+xs[2])
                    duration = aniso8601.parse_duration(xs[3])
                    maxtst = 10000
                    while maxtst > 0:
                        maxtst -= 1
                        try:
                            d = dates.next()
                        except:
                            return False
                        # print str(d),"--",str(d+duration)
                        if str(d) <= str(env["_now"]) <= str(d+duration):
                            return True
                    return False
                else:
                    dates = aniso8601.parse_repeating_interval(self.time)
                    if "test" in sys.argv:
                        print "REPEAT TIME",dates
                    maxtst = 10000
                    while maxtst > 0:
                        maxtst -= 1
                        try:
                            d = dates.next()
                        except:
                            return False
                        if str(env["_now"])[0:10] == str(d)[0:10]:
                            return True
                return False
            # Period
            elif 'P' in self.time:
                interval = sorted(aniso8601.parse_interval(self.time))
                if "test" in sys.argv:
                    print "INTERVAL",interval
                res = interval[0] <= env["_now"] <= interval[1]
                if "test" in sys.argv:
                    print "RESULT",res
                return res
            # Simple date
            else:
                if "test" in sys.argv:
                    print "SIMPLE DATE",env["_now"]," vs. ",self.time
                res = self.time[0:10].replace("-", "") == str(env["_now"])[0:10].replace("-", "")
                if "test" in sys.argv:
                    print "RESULT",res
                return res
        else:
            print "ERROR: Current time missing"
            sys.exit(-1)

    def toObject(self):
        return {"temporal": self.time}

# Evaluating a macro just amount to evaluating its expression in the current environment
class MacroReference(Condition):
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return 'MacroReference(%s)' % self.name

    def eval(self, env):
        if self.name in env:
            if "test" in sys.argv:
                print "EVALUATING MACRO",self.name
            res = env[self.name].eval(env)
            if "test" in sys.argv:
                print "RESULT",res
            return res
        else:
            print "ERROR: macro not defined:",self.name
            sys.exit(-1)

    def toObject(self):
        return {"macro": self.name}


# Conjunction
class And(Condition):
    def __init__(self, left, right):
        self.left = left
        self.right = right

    def __repr__(self):
        return 'AndBexp(%s, %s)' % (self.left, self.right)

    def eval(self, env):
        left_value = self.left.eval(env)
        right_value = self.right.eval(env)
        return left_value and right_value

    def toObject(self):
        return self.fromBinary("and")

# Disjunction
class Or(Condition):
    def __init__(self, left, right):
        self.left = left
        self.right = right

    def __repr__(self):
        return 'OrBexp(%s, %s)' % (self.left, self.right)

    def eval(self, env):
        left_value = self.left.eval(env)
        right_value = self.right.eval(env)
        return left_value or right_value

    def toObject(self):
        return self.fromBinary("or")

# Negation
class Not(Condition):
    def __init__(self, exp):
        self.exp = exp

    def __repr__(self):
        return 'NotBexp(%s)' % self.exp

    def eval(self, env):
        value = self.exp.eval(env)
        return not value

    def toObject(self):
        return {"not": self.exp.toObject()}
