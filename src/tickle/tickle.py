

import sys
from imp_parser import *
from imp_lexer import *
from imp_json import tickleFromJson
import isodate
import json

def usage():
    sys.stderr.write('Usage: python imp.py codefile testfile\n')
    sys.exit(1)

if __name__ == '__main__':
    if len(sys.argv) < 3:
        usage()
    filename = sys.argv[1]
    text = open(filename).read()
    tokens = imp_lex(text)
    parse_result = imp_parse(tokens)
    if not parse_result:
        sys.stderr.write('Parse error!\n')
        sys.exit(1)
    ast = parse_result.value
    f = open(sys.argv[2])
    tstcnt = 0
    tstok = 0
    for l in f.readlines():
        tstcnt += 1
        env = json.loads(l.strip())
        print l.strip()
        if "test" in sys.argv:
            print "ENV",env
        env["_now"] = isodate.parse_datetime(env["_datetime"])
        env["_result"] = False
        ast.eval(env)

        if "test" in sys.argv:
            sys.stdout.write('Final variable values:\n')
            for name in env:
                sys.stdout.write('%s: %s\n' % (name, env[name]))
        print "------------------------------------------------------------------------------------------"
        print "result:",env['_result'],"test result:",env['_result'] == env['_expect']
        print "=========================================================================================="
        if env['_result'] == env['_expect']:
            tstok += 1

    print tstcnt,"tests,",tstok,"ok"

    # Test to/from AST/JSON
    ast2 = tickleFromJson(ast.toJson())
    print "\nJSON/AST conversion succeeded: %s" % (ast.toJson() == ast2.toJson())
    print ast.toJson()

